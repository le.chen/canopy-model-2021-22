#' This is the main file for the complete canopy model


library(tidyverse)

#' Canopy model
#' This is the overall canopy model
#' @param input The input data
#' @param state The model state
#' @return list of `state` and `output`

canopy_model_step <- function(input, state){
  # Load input data
  output <- list(a=1)
  return(list(
    state = state,
    output = output
  ))
  
}

